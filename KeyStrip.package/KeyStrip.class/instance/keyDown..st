as yet unclassified
keyDown: event
	| string keyStrings pos |
	string := event keyString.
	"Transcript
		show: string;
		cr."
	keyStrings := { '<Opt-Cmd-!>'.'<Opt-Cmd-@>'.'<Opt-Cmd-#>'.'<Opt-Cmd-$>'.'<Opt-Cmd-%>'.'<Opt-Cmd-^>'.'<Opt-Cmd-&>'.'<Opt-Cmd-*>'} asOrderedCollection.
	pos := keyStrings indexOf: 	string startingAt: 1.
	pos > 0 ifTrue: [KeyStrip executeExpression: pos]. 