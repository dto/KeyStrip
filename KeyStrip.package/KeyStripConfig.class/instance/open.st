as yet unclassified
open
	self openInWindowLabeled: 'KeyStrip Configuration'.
	self fillExpressions: KeyStrip expressions.
	self ui updateButton label: 'Save Expressions'.
	(self ui updateButton) on: #click
		send: #saveExpressions
		to: self